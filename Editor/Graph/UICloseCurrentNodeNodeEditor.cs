﻿using com.fdt.statemachinegraph;
using com.fdt.statemachinegraph.editor;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace com.FDT.UISystem.Editor
{
    [NodeEditor.CustomNodeEditorAttribute(typeof(UICloseCurrentNode))]
    public class UICloseCurrentNodeNodeEditor : SMTimeNodeNodeEditor
    {
        protected override void DrawNodeContent()
        {
            base.DrawNodeContent();
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("exit"),
                new GUIContent("Exit"), true);
        }
    }
    [CustomEditor(typeof(UICloseCurrentNode))]
    public class UICloseCurrentNodeEditor : SMTimeNodeEditor
    {
        protected override void DrawContent(ISMPlayer cdata)
        {
            base.DrawContent(cdata);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_unityEvt"), true);
        }
    }
}