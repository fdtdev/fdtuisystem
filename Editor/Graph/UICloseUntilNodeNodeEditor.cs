﻿using System.Collections;
using System.Collections.Generic;
using com.fdt.statemachinegraph;
using com.fdt.statemachinegraph.editor;
using UnityEditor;
using UnityEngine;
using XNodeEditor;

namespace com.FDT.UISystem.Editor
{
    [NodeEditor.CustomNodeEditorAttribute(typeof(UICloseUntilNode))]
    public class UICloseUntilNodeNodeEditor : SMTimeNodeNodeEditor
    {
        protected override void DrawNodeContent()
        {
            base.DrawNodeContent();
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("_windowIdAsset"), true);
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("_inclusive"), true);
            NodeEditorGUILayout.PropertyField(serializedObject.FindProperty("exit"),
                new GUIContent("Exit"), true);
        }
    }
    [CustomEditor(typeof(UICloseUntilNode))]
    public class UICloseUntilNodeEditor : SMTimeNodeEditor
    {
        protected override void DrawContent(ISMPlayer cdata)
        {
            base.DrawContent(cdata);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_windowIdAsset"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_inclusive"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_unityEvt"), true);
        }
    }
}