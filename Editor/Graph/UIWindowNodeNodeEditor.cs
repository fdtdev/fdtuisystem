﻿using System.Collections;
using System.Collections.Generic;
using com.fdt.statemachinegraph;
using com.fdt.statemachinegraph.editor;
using UnityEditor;

namespace com.FDT.UISystem.Editor
{
    [CustomNodeEditor(typeof(UIWindowNode))]
    public class UIWindowNodeNodeEditor : SMNodeNodeEditor
    {
        protected override float LabelWidth
        {
            get { return 60; }
        }

        protected override void DrawNodeContent()
        {
            EditorGUILayout.Space();

            var windowProp = serializedObject.FindProperty("_uiwindowID");
            EditorGUILayout.PropertyField(windowProp, true);

            EditorGUILayout.Space();
            base.DrawNodeContent();
        }
    }

    [CustomEditor(typeof(UIWindowNode))]
    public class UIWindowNodeEditor : SMNodeEditor
    {

    }
}