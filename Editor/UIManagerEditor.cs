﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace com.FDT.UISystem.Editor
{
    [CustomPropertyDrawer(typeof(UIManager.UIData))]
    public class UIData : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            SerializedProperty loadTypeProp = property.FindPropertyRelative("loadType");
            GUI.Box(position, GUIContent.none);
            position.x += 2;
            position.y += 2;
            position.width -= 4;
            position.height -= 4;
            
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight),
                loadTypeProp, true);
            EditorGUI.PropertyField(new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + 2, position.width, EditorGUIUtility.singleLineHeight),
                property.FindPropertyRelative("windowID"), true);
            switch (loadTypeProp.intValue)
            {
                case (int)UIManager.LoadType.ADDRESSABLES:
                    EditorGUI.PropertyField(new Rect(position.x, position.y + ((EditorGUIUtility.singleLineHeight + 2) * 2), position.width, EditorGUIUtility.singleLineHeight),
                        property.FindPropertyRelative("uiWindowPrefab"), true);
                    break;
                case (int)UIManager.LoadType.RESOURCES:
                    EditorGUI.PropertyField(new Rect(position.x, position.y + ((EditorGUIUtility.singleLineHeight + 2) * 2), position.width, EditorGUIUtility.singleLineHeight),
                        property.FindPropertyRelative("uiWindowPrefabResource"), true);
                    break;
            }
            
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return ((EditorGUIUtility.singleLineHeight + 2) * 3)+4;
        }
    }
    
}
