# FDT UI System

UISystem is a node system for ui windows and popups management



## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.uisystem": "https://bitbucket.org/fdtdev/fdtuisystem.git#1.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",
	"com.fdt.statemachinegraph": "https://bitbucket.org/fdtdev/fdtstatemachinegraph.git#3.0.0",
	"com.fdt.genericdatasaving": "https://bitbucket.org/fdtdev/fdtgenericdatasaving.git#4.0.0",
	"com.fdt.gameevents": "https://bitbucket.org/fdtdev/fdtgameevents.git#2.0.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtuisystem/src/1.0.0/LICENSE.md)