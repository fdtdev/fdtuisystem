﻿using System;
using com.fdt.genericdatasaving;
using com.fdt.statemachinegraph;
using UnityEngine;

namespace com.FDT.UISystem
{
    [CreateAssetMenu(menuName = "FDT/UISystem/Graph/UICacheSetting", fileName = "UICacheSetting", order = 0)]
    public class UICacheSetting : StateSettingAsset
    {
        public struct Param1 : IContentParameter
        {
            public VarType GetVarType
            {
                get { return VarType.ASSET; }
            }
            public Type GetAssetType
            {
                get { return typeof(UIWindowIDAsset); }
            }
            public bool GetTypeFromAsset
            {
                get { return true; }
            }

            public string GetCustomDescription
            {
                get { return "windowID"; }
            }
        }
        protected IContentParameter[] _params = { new Param1()};
        public override IContentParameter[] Parameters
        {
            get { return _params; }
        }
        public override void EnterExecutionRange(int contentIdx, SMNodeBase smNode, ISMPlayer cdata, ValueData[] _valueDatas, TimeRangeData timeRange,
            float time, float nTime)
        {
            UIWindowIDAsset windowID = _valueDatas[0].AssetValue as UIWindowIDAsset;
            UIHandler _uihandlerID = cdata as UIHandler;
            UIManager.Instance.CacheUI(windowID, _uihandlerID, cdata);
        }
    }
}