﻿using com.fdt.statemachinegraph;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.UISystem
{
    [NodeWidth(140), NodeTint(0.6f, 0.2f, 0.2f)]
    public class UICloseCurrentNode : SMTimeNode
    {
        [SerializeField] protected UnityEvent _unityEvt;
        
        public override bool HasTime
        {
            get { return false; }
        }

        public override void Enter(ISMPlayer cdata)
        {
            base.Enter(cdata);
            _unityEvt.Invoke();
            cdata.SetValue(UIManager.RunningCloseNode, true);
            UIHandler _uihandlerID = cdata as UIHandler;
            var async = UIManager.Instance.CloseCurrentUI(_uihandlerID, cdata);
            async.OnComplete += s =>
            {
                cdata.SetValue(UIManager.RunningCloseNode, false);
            };
        }

        public override SMNodeBase GetNextNode(ISMPlayer cdata)
        {
            bool v = cdata.GetBool(UIManager.RunningCloseNode);
            if (!v)
            {
                var p = GetPort("exit");
                if (p.ConnectionCount > 0)
                {
                    return (p.GetConnection(0).node as SMNodeBase).GetNode();
                }
            }

            return null;
        }
    }
}