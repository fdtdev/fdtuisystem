﻿using com.fdt.statemachinegraph;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.UISystem
{
    [NodeWidth(220), NodeTint(0.2f, 0.6f, 0.2f)]
    public class UICloseUntilNode : SMTimeNode
    {
        [SerializeField] protected UnityEvent _unityEvt;
        [SerializeField] protected UIWindowIDAsset _windowIdAsset;
        [SerializeField] protected bool _inclusive;
        
        public override bool HasTime
        {
            get { return false; }
        }

        public override void Enter(ISMPlayer cdata)
        {
            base.Enter(cdata);
            _unityEvt.Invoke();
            cdata.SetValue(UIManager.RunningCloseNode, true);
            UIHandler _uihandlerID = cdata as UIHandler;
            var async = UIManager.Instance.CloseUntil(_windowIdAsset, _uihandlerID, _inclusive, cdata);
            async.OnComplete += s =>
            {
                cdata.SetValue(UIManager.RunningCloseNode, false);
            };
        }

        public override SMNodeBase GetNextNode(ISMPlayer cdata)
        {
            bool v = cdata.GetBool(UIManager.RunningCloseNode);
            if (!v)
            {
                var p = GetPort("exit");
                if (p.ConnectionCount > 0)
                {
                    return ((SMNodeBase) p.GetConnection(0).node).GetNode();
                }
            }

            return null;
        }
    }
}