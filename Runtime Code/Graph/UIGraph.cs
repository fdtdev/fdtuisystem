﻿using com.fdt.statemachinegraph;
using UnityEngine;

namespace com.FDT.UISystem
{
    [CreateAssetMenu(menuName = "FDT/UISystem/Graph/UIGraph", fileName = "UIGraph", order = 0)]
    public class UIGraph : SMGraph
    {
        public override void CreateCachedNamespaces()
        {
            _defaultNodeNamespaces = new string[2] {"com.fdt.statemachinegraph.common", "com.FDT.UISystem"};
        }
    }
}