﻿using System;
using com.fdt.genericdatasaving;
using com.fdt.statemachinegraph;
using UnityEngine;

namespace com.FDT.UISystem
{
    [CreateAssetMenu(menuName = "FDT/UISystem/Graph/UINavigationCondition", fileName = "UINavigationCondition", order = 0)]
    public class UINavigationCondition : ConditionsAsset
    {
        public struct Param1 : IContentParameter
        {
            public VarType GetVarType
            {
                get { return VarType.ASSET; }
            }
            public Type GetAssetType
            {
                get { return typeof(UINavigationIDAsset); }
            }
            public bool GetTypeFromAsset
            {
                get { return true; }
            }

            public string GetCustomDescription
            {
                get { return "navigationID"; }
            }
        }
        protected IContentParameter[] _params = { new Param1()};
        public override IContentParameter[] Parameters
        {
            get { return _params; }
        }
        public override bool OnCheckCondition(int contentIdx, SMNodeBase n, ISMPlayer cdata, ValueData[] _valueDatas, TimeRangeData timeRange, float time,
            float nTime)
        {
            UINavigationIDAsset navIDA = _valueDatas[0].AssetValue as UINavigationIDAsset;
            UINavigationIDAsset navIDB = cdata.GetValue(UIManager.Navigation) as UINavigationIDAsset;

            bool v = navIDA == navIDB;
            if (v)
            {
                cdata.SetValue(UIManager.Navigation, null);
            }

            return v;
        }
    }
}