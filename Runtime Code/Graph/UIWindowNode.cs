﻿using com.fdt.statemachinegraph;
using UnityEngine;

namespace com.FDT.UISystem
{
    [NodeWidth(250)]
    public class UIWindowNode : SMNode
    {
        public override bool HasTime
        {
            get { return false; }
        }

        [SerializeField] protected UIWindowIDAsset _uiwindowID;
        

        public override void Enter(ISMPlayer cdata)
        {
            base.Enter(cdata);
            UIHandler _uihandlerID = cdata as UIHandler;
            UIManager.Instance.LoadAndShowUI(_uiwindowID, _uihandlerID, cdata);
        }
    }
}