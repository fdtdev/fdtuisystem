﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.UISystem
{
    [CreateAssetMenu(menuName = "FDT/UISystem/UIHandlerIDAsset", fileName = "UIHandlerIDAsset", order = 0)]
    public class UIHandlerIDAsset : IDAsset
    {
       
    }
}