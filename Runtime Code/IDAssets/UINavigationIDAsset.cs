﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.UISystem
{
    [CreateAssetMenu(menuName = "FDT/UISystem/UINavigationIDAsset", fileName = "UINavigationIDAsset", order = 0)]
    public class UINavigationIDAsset : IDAsset
    {

    }
}