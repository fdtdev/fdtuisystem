﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.UISystem
{
    [CreateAssetMenu(menuName = "FDT/UISystem/UIWindowIDAsset", fileName = "UIWindowIDAsset", order = 0)]
    public class UIWindowIDAsset : IDAsset
    {
       
    }
}