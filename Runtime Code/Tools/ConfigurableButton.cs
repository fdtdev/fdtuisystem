﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.UISystem
{
    public abstract class ConfigurableButton<T> : MonoBehaviour where T : UIItemConfigBase
    {
        public virtual void Init(T data)
        {
            
        }
    }
}