﻿using System;
using com.FDT.Common;
using com.fdt.statemachinegraph;
using UnityEngine;

namespace com.FDT.UISystem
{
    public class UIAsyncOp: AsyncOpBase<UIAsyncOp>
    {
        public bool IsLoaded = false;
        public bool IsCreated = false;
        public ISMPlayer cdata;
        public string prefab;
        public UIWindow instance;
        private UIHandler _handler;
        public UIHandler handler
        {
            get { return _handler; }
            set
            {
                _handler = value;
            }
        }

        protected override void OnIsDoneChanged()
        {
            if (_isDone && OnComplete != null)
            {
                OnComplete.Invoke(prefab);
            }
        }

        public override bool keepWaiting
        {
            get
            {
                return !IsDone;
            }
        }
    }
}