﻿using System;
using System.Collections.Generic;
using com.FDT.Common;
using com.fdt.statemachinegraph;
using UnityEngine;

namespace com.FDT.UISystem
{
    public class UIHandler : SMPlayer,
        IRegistrable<UIHandlerIDAsset>
    {
        [SerializeField] protected bool _isDefault = true;
        public bool IsDefault
        {
            get { return _isDefault; }
        }
        [SerializeField] protected List<UIWindow> _currentOpenedUIs = new List<UIWindow>();
        [SerializeField] protected List<UIWindowIDAsset> _currentOpenedUIIDs = new List<UIWindowIDAsset>();
        [SerializeField] protected UIHandlerIDAsset _referenceID;

        protected override void OnEnable()
        {
            UIManager.Instance.Register(this);
            base.OnEnable();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            if (UIManager.Exists)
            {
                UIManager.Instance.Unregister(this);    
            }
        }

        public UIHandlerIDAsset referenceID
        {
            get { return _referenceID; }
        }

        public void AddUI(UIWindow ui)
        {
            _currentOpenedUIs.Insert(0, ui);
            _currentOpenedUIIDs.Insert(0, ui.WindowID);
        }

        public void RefreshActiveUI()
        {
            if (_currentOpenedUIs.Count > 0)
            {
                _currentOpenedUIs[0].ActivateInput();
                if (_currentOpenedUIs.Count > 1)
                {
                    for (int i = 1; i <= _currentOpenedUIs.Count - 1; i++)
                    {
                        _currentOpenedUIs[i].DeactivateInput();
                    }
                }
            }
        }
        public UIWindow GetCurrentInstance()
        {
            if (_currentOpenedUIs.Count > 0)
            {
                return _currentOpenedUIs[0];
            }

            return null;
        }
        public UIWindowIDAsset GetCurrentID()
        {
            if (_currentOpenedUIIDs.Count > 0)
            {
                return _currentOpenedUIIDs[0];
            }

            return null;
        }
        public bool DestroyId(UIWindowIDAsset id)
        {
            int idx = _currentOpenedUIIDs.IndexOf(id);
                
            if (idx < 0) return false;
            
            return DestroyIdx(idx);
        }
        public bool DestroyIdx(int idx)
        {
            if (_currentOpenedUIs.Count > 0)
            {
                _currentOpenedUIIDs.RemoveAt(idx);
                _currentOpenedUIs.RemoveAt(idx);
                
                return true;
            }
            return false;
        }

        public List<UIWindow> GetInstancesUntil(UIWindowIDAsset wId, bool inclusive)
        {
            List<UIWindow> result = new List<UIWindow>();
            for (int i = 0; i < _currentOpenedUIIDs.Count; i++)
            {
                if (_currentOpenedUIIDs[i] != wId)
                {
                    result.Add(_currentOpenedUIs[i]);
                }
                else
                {
                    if (inclusive)
                    {
                        result.Add(_currentOpenedUIs[i]);
                    }

                    break;
                }
            }

            return result;
        }

        public void DeactivateAllInputs()
        {

            if (_currentOpenedUIs.Count > 0)
            {
                for (int i = 0; i <= _currentOpenedUIs.Count - 1; i++)
                {
                    _currentOpenedUIs[i].DeactivateInput();
                }
            }
        }

        public void DoNavigation(UINavigationIDAsset navID)
        {
            if (navID == GetCurrentID())
                return;
            DeactivateAllInputs();
            SetValue(UIManager.Navigation, navID);
        }
    }
}