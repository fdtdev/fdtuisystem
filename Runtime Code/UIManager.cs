﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.FDT.Common;
using com.fdt.statemachinegraph;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace com.FDT.UISystem
{
    public class UIManager : Singleton<UIManager>, IRegisterer<UIHandler, UIHandlerIDAsset>
    {
        public const string RunningCloseNode = "runningCloseNode";
        public const string Navigation = "nav";
        
        protected Transform cacheParent;

        public enum LoadType
        {
            ADDRESSABLES = 0, RESOURCES = 1
        }
        [System.Serializable]
        public class UIData
        {
            public LoadType loadType;
            public UIWindowIDAsset windowID;
            public AssetReference uiWindowPrefab;
            [ResourceReferenceAttribute] public string uiWindowPrefabResource;
        }
        
        [SerializeField] protected List<UIData> _uiDatas = new List<UIData>();
        protected Dictionary<UIWindowIDAsset, UIData> _uiDataCache = new Dictionary<UIWindowIDAsset, UIData>();
        
        protected Dictionary<UIHandlerIDAsset, UIHandler> registered = new Dictionary<UIHandlerIDAsset, UIHandler>();

        protected Dictionary<UIWindowIDAsset, UIWindow> cached = new Dictionary<UIWindowIDAsset, UIWindow>();

        protected List<UIWindowIDAsset> beingCached = new List<UIWindowIDAsset>();
        protected UIHandler _defaultUIHandler;
        public UIHandler DefaultUIHandler
        {
            get { return _defaultUIHandler; }
        }

        protected override void OnAwake()
        {
            cacheParent = new GameObject("cacheParent").transform;
            cacheParent.SetParent(transform);
            CacheUIDatas();
        }

        protected void CacheUIDatas()
        {
            if (_uiDataCache.Count > 0)
                return;
            
            for (int i = 0; i < _uiDatas.Count; i++)
            {
                var uiData = _uiDatas[i];
                _uiDataCache.Add(uiData.windowID, uiData);
            }
        }

        public UIHandler GetUIHandler(UIHandlerIDAsset handlerID)
        {
            return registered[handlerID];
        }
        protected UIData GetUIData(UIWindowIDAsset idAsset)
        {
            return _uiDataCache[idAsset];
        }
        
        public void Register(UIHandler registeredItem, Action onRegistrationSucceed = null)
        {
            registered.Add(registeredItem.referenceID, registeredItem);
            if (registeredItem.IsDefault)
            {
                if (_defaultUIHandler != null)
                {
                    Debug.LogWarning($"Default UI handler already set ({_defaultUIHandler.name}) trying to set it to {registeredItem.name}.");
                }
                _defaultUIHandler = registeredItem;
            }
        }

        public void Unregister(UIHandler registeredItem, Action onUnregistrationSucceed = null)
        {
            registered.Remove(registeredItem.referenceID);
            if (_defaultUIHandler == registeredItem)
            {
                _defaultUIHandler = null;
            }
        }

        private void HandleCompleted(string obj)
        {
            RefreshActiveUI();
        }

        protected void RefreshActiveUI()
        {
            foreach (var item in registered)
            {
                item.Value.RefreshActiveUI();
            }
        }
       
        public UIAsyncOp LoadAndShowUI(UIWindowIDAsset uiwindowID, UIHandler uihandler, ISMPlayer cdata)
        {
            UIAsyncOp w = new UIAsyncOp
            {
                IsLoaded = false,
                IsDone = false,
                prefab = null,
                handler = uihandler,
                cdata =  cdata
            };
            var currentUI = uihandler.GetCurrentID();
            if (currentUI != null && currentUI == uiwindowID)
            {
                w.IsDone = true;
                return w;
            }
            
            StartCoroutine(DoLoadAndShowUI(uiwindowID, w));
            w.SetOnComplete(HandleCompleted);
            return w;
        }

        private IEnumerator DoLoadAndShowUI(UIWindowIDAsset uiwindow, UIAsyncOp w)
        {
            if (!beingCached.Contains(uiwindow))
            {
                yield return StartCoroutine(DoLoad(uiwindow, w));
            }
            else
            {
                do
                {
                    yield return null;
                } 
                while (beingCached.Contains(uiwindow));
            }

            yield return StartCoroutine(DoShow(uiwindow, w));
            w.IsDone = true;
        }

        private IEnumerator DoLoad(UIWindowIDAsset uiwindow, UIAsyncOp w)
        {
            if (!cached.ContainsKey(uiwindow))
            {
                beingCached.Add(uiwindow);
                
                CacheUIDatas();
                var uiWindowAssetRef = _uiDataCache[uiwindow];
                UIWindow prefab = null;
                bool success = false;
                if (uiWindowAssetRef.loadType == LoadType.ADDRESSABLES)
                {
                    var async = Addressables.LoadAssetAsync<GameObject>(uiWindowAssetRef.uiWindowPrefab);
                    yield return async;
                    if (async.Status != AsyncOperationStatus.Failed)
                    {
                        success = true;
                        prefab = async.Result.GetComponent<UIWindow>();
                    }
                }
                else
                {
                    var async = Resources.LoadAsync<GameObject>(uiWindowAssetRef.uiWindowPrefabResource);
                    yield return async;
                    if (async.asset != null)
                    {
                        success = true;
                        prefab = (async.asset as GameObject).GetComponent<UIWindow>();
                    }
                }
                if (success)
                {
                    
                    prefab.gameObject.SetActive(false);
                    prefab.WindowID = uiwindow;
                    prefab = Instantiate(prefab, w.handler.transform, false);
                    cached.Add(uiwindow, prefab);
                    if (prefab.AsyncCreation)
                    {
                        yield return prefab.CreationAsync();
                    }
                    else
                    {
                        prefab.Creation();
                    }
                }
                beingCached.Remove(uiwindow);
            }
            yield return null;
        }
        private IEnumerator DoShow(UIWindowIDAsset uiwindow, UIAsyncOp w)
        {
            UIWindow prefab = cached[uiwindow];
            UIWindow ui = w.instance = prefab;
            ui.WindowID = uiwindow;
            //w.instance.transform.SetParent(w.handler.transform, false);
            
            w.instance.gameObject.SetActive(true);
            w.instance.DeactivateInput();
            
            w.instance.transform.SetAsLastSibling();
            ui.cdata = w.cdata;
            w.handler.AddUI(ui);
            if (ui.HasAsyncIn)
            {
                yield return ui.ShowAsync(w.handler);
            }
            else
            {
                ui.Show(w.handler);
            }
            w.instance.ActivateInput();
        }
        
        public UIAsyncOp CloseCurrentUI(UIHandler uiHandler, ISMPlayer cdata)
        {
            UIAsyncOp w = new UIAsyncOp
            {
                IsLoaded = false,
                IsDone = false,
                prefab = null,
                handler = uiHandler
            };
            UIWindow ui = uiHandler.GetCurrentInstance();
            StartCoroutine(doCloseUI(ui, 0,  uiHandler, w));
            return w;
        }
        protected IEnumerator doCloseUI(UIWindow ui, int idx, UIHandler uiHandler, UIAsyncOp w)
        {
            ui.DeactivateInput();
            if (ui.HasAsyncOut)
            {
                yield return ui.HideAsync();
            }
            else
            {
                ui.Hide();
            }
            ui.gameObject.SetActive(false);
            uiHandler.DestroyIdx(idx);
            uiHandler.RefreshActiveUI();
            w.IsDone = true;
        }
        //----
        public UIAsyncOp CloseUntil(UIWindowIDAsset wId, UIHandler uiHandler, bool inclusive, ISMPlayer cdata)
        {
            UIAsyncOp w = new UIAsyncOp
            {
                IsLoaded = false,
                IsDone = false,
                prefab = null,
                handler = uiHandler
            };
            StartCoroutine(doCloseUntil(wId, uiHandler, inclusive, w));
            return w;
        }
        protected IEnumerator doCloseUntil(UIWindowIDAsset wId, UIHandler uiHandler, bool inclusive, UIAsyncOp w)
        {
            List<UIWindow> uis = uiHandler.GetInstancesUntil(wId, inclusive);
            List<UIAsyncOp> results = new List<UIAsyncOp>();
            for (int i = uis.Count -1; i >=0 ; i--)
            {
                UIAsyncOp ao = new UIAsyncOp();
                StartCoroutine(doCloseUI(uis[i], i, uiHandler, ao));
                results.Add(ao);
            }

            bool finished = false;
            do
            {
                yield return null;
                finished = true;
                for (int i = 0; i < results.Count; i++)
                {
                    if (!results[i].IsDone)
                    {
                        finished = false;
                        break;
                    }
                }
                
            } while (!finished);
            w.IsDone = true;
        }
        
        public UIAsyncOp CacheUI(UIWindowIDAsset uiwindowID, UIHandler uihandler, ISMPlayer cdata)
        {
            UIAsyncOp w = new UIAsyncOp
            {
                IsLoaded = false,
                IsDone = false,
                prefab = null,
                handler = uihandler,
                cdata =  cdata
            };
            StartCoroutine(DoCacheUI(uiwindowID, w));
            return w;
        }

        private IEnumerator DoCacheUI(UIWindowIDAsset uiwindow, UIAsyncOp w)
        {
            if (!beingCached.Contains(uiwindow))
            {
                yield return StartCoroutine(DoLoad(uiwindow, w));
            }
            else
            {
                do
                {
                    yield return null;
                } 
                while (beingCached.Contains(uiwindow));
            }
            w.IsDone = true;
        }

        public void RemoveCached(UIWindowIDAsset uiwindow)
        {
            if (cached.ContainsKey(uiwindow))
            {
                var w = cached[uiwindow];
                DefaultUIHandler.DestroyId(uiwindow);
                cached.Remove(uiwindow);
                Destroy(w.gameObject);
            }
            else if (beingCached.Contains(uiwindow))
            {
                Debug.LogError($"requested {uiwindow} for deleting is still being cached");
            }
        }
    }
}