﻿using System;
using System.Collections;
using com.fdt.statemachinegraph;
using UnityEngine;

namespace com.FDT.UISystem
{
    [RequireComponent(typeof(CanvasGroup))]
    public abstract class UIWindow : MonoBehaviour
    {
        public UIWindowIDAsset WindowID { get; set; }
        [SerializeField] protected UINavigationIDAsset _backButtonNavigationID;
        
        protected UIHandler _handler;
        [SerializeField] protected CanvasGroup _canvasGroup;
        public ISMPlayer cdata;
        public CanvasGroup CanvasGroup => _canvasGroup;

        protected bool _inputActive = false;
        
        protected virtual void Reset()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
        }        

        public virtual bool AsyncCreation
        {
            get {  return false; }
        }
        public virtual IEnumerator CreationAsync()
        {
            throw new NotImplementedException();
        }

        public virtual void Creation()
        {
            
        }
        //---------------------
        public virtual bool HasAsyncIn
        {
            get {  return false; }
        }
        public IEnumerator ShowAsync(UIHandler uiHandler)
        {
            _handler = uiHandler;
            yield return StartCoroutine(OnShowAsync());
        }
        protected virtual IEnumerator OnShowAsync()
        {
            throw new NotImplementedException();
        }
        public void Show(UIHandler uiHandler)
        {
            _handler = uiHandler;
            OnShow();
        }
        protected virtual void OnShow()
        {
            throw new NotImplementedException();
        }

        //---------------------
        
        
        public virtual bool HasAsyncOut
        {
            get {  return false; }
        }
        public IEnumerator HideAsync()
        {
            yield return StartCoroutine(OnHideAsync());
        }
        protected virtual IEnumerator OnHideAsync()
        {
            throw new NotImplementedException();
        }
        public void Hide()
        {
            OnHide();
        }
        protected virtual void OnHide()
        {
            throw new NotImplementedException();
        }
        //---------------------
        protected virtual void OnEnable()
        {
            
        }

        protected virtual void OnDisable()
        {
            
        }

        public void DoNavigation(UINavigationIDAsset navID)
        {
            _handler.DoNavigation(navID);
            
        }

        protected virtual void DoUpdate()
        {
            
        }
        protected void Update()
        {
            if (_inputActive && _backButtonNavigationID != null)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    DoNavigation(_backButtonNavigationID);
                }
            }
            DoUpdate();
        }

        public void DeactivateInput()
        {
            _inputActive = false;
           // _canvasGroup.blocksRaycasts = false;
            _canvasGroup.interactable = false;
            DoDeactivateInput();
        }
        protected virtual void DoDeactivateInput()
        {
            
        }
        public void ActivateInput()
        {
            _inputActive = true;
            //_canvasGroup.blocksRaycasts = true;
            _canvasGroup.interactable = true;
            DoActivateInput();
        }
        protected virtual void DoActivateInput()
        {
            
        }
    }
}